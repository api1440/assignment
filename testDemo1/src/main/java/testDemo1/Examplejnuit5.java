package testDemo1;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Examplejnuit5 {
	@BeforeAll
	public static void a() { 
		System.out.println("before all function");
	}
	@AfterAll
	public static void bc() {
		System.out.println("After all function ");
	}
	@BeforeEach
	public void c() {
		System.out.println("Before each function ");
	}
	@AfterEach
	public void ca() {
		System.out.println("After each function ");
	}
	@Test
	public void abc() {
		System.out.println("Test Case 1");
	}
	@Test
	public void ab() {
		System.out.println("Test Case 2");
	}
}
